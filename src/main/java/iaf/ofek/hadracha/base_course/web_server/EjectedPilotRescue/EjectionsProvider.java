package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import java.util.List;

public interface EjectionsProvider {
  List<EjectedPilotInfo> getEjections();
  void applyResponsibility(int injectionId, String clientId);
}
