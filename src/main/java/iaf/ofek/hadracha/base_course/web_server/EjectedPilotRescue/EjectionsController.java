package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/ejectedPilotRescue")
public class EjectionsController {

  @Autowired
  private EjectionsProvider provider;

  @GetMapping("/infos")
  private List<EjectedPilotInfo> getEjectedPilots() {
    return provider.getEjections();
  }

  @GetMapping("/takeResponsibility")
  private void takeResponsibility(@RequestParam int ejectionId, @CookieValue("client-id") String clientId) {
    provider.applyResponsibility(ejectionId, clientId);
  }
}
