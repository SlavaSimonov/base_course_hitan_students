package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.Data.CrudDataBase;
import iaf.ofek.hadracha.base_course.web_server.Data.Entity;
import iaf.ofek.hadracha.base_course.web_server.Utilities.ListOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class EjectionsImporter implements EjectionsProvider {

    @Value("${ejections.server.url}")
    public String EJECTION_SERVER_URL;
    @Value("${ejections.namespace}")
    public String NAMESPACE;

    @Autowired
    private AirplanesAllocationManager airplanes;

    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    private final RestTemplate restTemplate;
    private final CrudDataBase dataBase;
    private final ListOperations listOperations;
    private static final Double SHIFT_NORTH = 1.7;

    public EjectionsImporter(RestTemplateBuilder restTemplateBuilder, CrudDataBase dataBase, ListOperations listOperations) {
        restTemplate = restTemplateBuilder.build();
        this.dataBase = dataBase;
        this.listOperations = listOperations;
        executor.scheduleAtFixedRate(this::updateEjections, 1, 1, TimeUnit.SECONDS);

    }

    private void updateEjections() {
        try {
            List<EjectedPilotInfo> updatedEjections = getEjectionsFromServer();
            List<EjectedPilotInfo> previousEjections = dataBase.getAllOfType(EjectedPilotInfo.class);

            List<EjectedPilotInfo> addedEjections = ejectionsToHandle(updatedEjections, previousEjections);
            List<EjectedPilotInfo> removedEjections = ejectionsToHandle(previousEjections, updatedEjections);

            addedEjections.forEach(dataBase::create);
            removedEjections.stream().map(EjectedPilotInfo::getId).forEach(id -> dataBase.delete(id, EjectedPilotInfo.class));

        } catch (RestClientException e) {
            System.err.println("Could not get ejections: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private List<EjectedPilotInfo> ejectionsToHandle(List<EjectedPilotInfo> list1, List<EjectedPilotInfo> list2) {
        return listOperations.subtract(list1, list2, new Entity.ByIdEqualizer<>());
    }

    private List<EjectedPilotInfo> getEjectionsFromServer(){
        List<EjectedPilotInfo> ejectionsFromServer;
        ResponseEntity<List<EjectedPilotInfo>> responseEntity = initializeResponseEntity();
        ejectionsFromServer = responseEntity.getBody();
        if (ejectionsFromServer != null) {
            for(EjectedPilotInfo ejectedPilotInfo: ejectionsFromServer) {
                ejectedPilotInfo.getCoordinates().lat += SHIFT_NORTH;
            }
        }
        return ejectionsFromServer;
    }

    private ResponseEntity<List<EjectedPilotInfo>> initializeResponseEntity(){
        return restTemplate.exchange(
                EJECTION_SERVER_URL + "/ejections?name=" + NAMESPACE, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<EjectedPilotInfo>>() {
                });
    }

    @Override
    public List<EjectedPilotInfo> getEjections() {
        return this.dataBase.getAllOfType(EjectedPilotInfo.class);
    }

    @Override
    public void applyResponsibility(int injectionId, String clientId) {
        EjectedPilotInfo pilot = this.dataBase.getByID(injectionId, EjectedPilotInfo.class);
        if (pilot != null) {
            if(pilot.getRescuedBy() == null){
                pilot.setRescuedBy(clientId);
                dataBase.update(pilot);
                airplanes.allocateAirplanesForEjection(pilot, clientId);
            }
        }
    }
}
